<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
 <title>Address Management Application</title>
</head>
<body>
 <center>
  <h1>Address Management</h1>
        <h2>
         <a href="new">Add New Address</a>
         &nbsp;&nbsp;&nbsp;
         <a href="list">List All Addresss</a>

        </h2>
 </center>
    <div align="center">
  <c:if test="${address != null}">
   <form action="addresses?action=update" method="post">
        </c:if>
        <c:if test="${address == null}">
   <form action="addresses?action=insert" method="post">
        </c:if>
        <table border="1" cellpadding="5">
            <caption>
             <h2>
              <c:if test="${address != null}">
               Edit Address
              </c:if>
              <c:if test="${address == null}">
               Add New Address
              </c:if>
             </h2>
            </caption>
          <c:if test="${address != null}">
           <input type="hidden" name="id" value="<c:out value='${address.id}' />" />
          </c:if>
            <tr>
                <th>Address Country: </th>
                <td>
                 <input type="text" name="country" size="45"
                   value="<c:out value='${address.country}' />"
                  />
                </td>
            </tr>
            <tr>
                <th>Address City: </th>
                <td>
                 <input type="text" name="city" size="45"
                   value="<c:out value='${address.city}' />"
                  />
                </td>
            </tr>
            <tr>
                <th>Address Postal Code: </th>
                <td>
                 <input type="text" name="postalCode" size="45"
                   value="<c:out value='${address.postalCode}' />"
                  />
                </td>
            </tr>
            <tr>
                <th>Address Street: </th>
                <td>
                 <input type="text" name="street" size="45"
                   value="<c:out value='${address.street}' />"
                  />
                </td>
            </tr>
            <tr>
                <th>Address Number: </th>
                <td>
                 <input type="text" name="number" size="45"
                   value="<c:out value='${address.number}' />"
                  />
                </td>
            </tr>
            <tr>
             <td colspan="2" align="center">
              <input type="submit" value="Save" />
             </td>
            </tr>
        </table>
        </form>
    </div>
</body>
</html>