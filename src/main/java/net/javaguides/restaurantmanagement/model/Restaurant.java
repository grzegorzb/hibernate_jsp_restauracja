package net.javaguides.restaurantmanagement.model;

import javax.persistence.*;

@Entity
@Table
public class Restaurant {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "restaurantName")
    private String restaurantName;

    @Column(name = "address")
    private String address;

    public Restaurant() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
