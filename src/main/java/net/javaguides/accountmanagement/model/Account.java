package net.javaguides.accountmanagement.model;

import javax.persistence.*;

@Entity
@Table(name = "accout")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;


    public Account() {
    }

    public Account withId(int id) {
        setId(id);
        return this;
    }

    public Account build() {
        //action
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
