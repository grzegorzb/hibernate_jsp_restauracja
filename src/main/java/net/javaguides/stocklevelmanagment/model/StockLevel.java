package net.javaguides.stocklevelmanagment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;



/**
 * StanMagazynowy.java
 * This is a model class represents a StanMagazynowy entity
 * @author Grzegorz
 *
 */

@Entity
@Table(name="stock_level")
public class StockLevel {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    protected int id;

    @Column(name="unit_desc")
    protected String unitDesc;

    @Column(name="amount")
    protected int amount;

//    @OneToOne(cascade = CascadeType.ALL)
//    EmailAddress emailAddress;
//
//    @OneToOne(cascade = CascadeType.ALL)
//    EmailAddress additionalEmailAddress;

    public StockLevel() {
    }

    public net.javaguides.stocklevelmanagment.model.StockLevel withUnitDesc(String unit_desc){
        setUnitDesc(unit_desc);
        return this;
    }

    public net.javaguides.stocklevelmanagment.model.StockLevel withAmount(int amount){
        setAmount(amount);
        return this;
    }

    public net.javaguides.stocklevelmanagment.model.StockLevel withId(int id) {
        setId(id);
        return this;
    }

    public net.javaguides.stocklevelmanagment.model.StockLevel build(){
        //actions on properties
        return this;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getUnitDesc() {
        return unitDesc;
    }
    public void setUnitDesc(String unit_desc) {
        this.unitDesc = unit_desc;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        net.javaguides.stocklevelmanagment.model.StockLevel stockLevel = (net.javaguides.stocklevelmanagment.model.StockLevel) o;
        return id == stockLevel.id &&
                unitDesc.equals(stockLevel.unitDesc) &&
                amount==stockLevel.amount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, unitDesc, amount);
    }
}