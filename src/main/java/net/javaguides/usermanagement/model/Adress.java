package net.javaguides.usermanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User.java
 * This is a model class represents a User entity
 *
 * @author Ramesh Fadatare
 */

@Entity
@Table(name = "adresses")
public class Adress {

    //fluent api(with)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected int id;

    @Column(name = "city")
    protected String city;

    @Column(name = "street")
    protected String street;

    @Column(name = "postalCode")
    protected String country;

    @Column(name = "number")
    protected String number;

    public Adress() {
    }

    public Adress withCountry(String country) {
        this.country = country;
        return this;
    }

    public Adress withCity(String city) {
        setCity(city);
        return this;
    }

    public Adress withStreet(String street) {
       setStreet(street);
        return this;
    }





    public Adress withNumber(String number) {
        setNumber(number)     ;
        return this;
    }




    public Adress(String city, String street, String country, String number) {
        this.city = city;
        this.street = street;
        this.country = country;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public Adress setId(int id) {
        this.id = id;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Adress setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public Adress setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Adress setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getNumber() {
        return number;
    }

    public Adress setNumber(String number) {
        this.number = number;
        return this;
    }
}
