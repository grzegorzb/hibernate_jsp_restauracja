package net.javaguides.menumenagement.model;

import net.javaguides.emailmanagement.model.EmailAddress;

import javax.persistence.*;

/**
 * Menu.java
 * This is a model class represents a User entity
 * @author Ramesh Fadatare
 *
 */

@Entity
@Table(name="menus")
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected int id;

    @Column(name = "name")
    protected String name;

    public Menu() {
    }

    public Menu withName(String name) {
        setName(name);
        return this;
    }

    public Menu withId(int id) {
        setId(id);
        return this;
    }

    public Menu build() {
        //actions on properties
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}