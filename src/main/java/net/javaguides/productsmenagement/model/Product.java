package net.javaguides.productsmenagement.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "product")
public class Product {


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    protected int id;

    @Column(name = "name")
    protected String name;

    @Column(name = "productPrice")
    protected double productPrice;

    public Product() {
    }

    public Product withId(int Id) {
        setId(id);
        return this;
    }

    public Product withName(String name) {
        setName(name);
        return this;
    }

    public Product withProductPrice(double productPrice) {
        setProductPrice(productPrice);
        return this;
    }

    public Product build(){
        return this;
    }


    public int getId() {
        return id;
    }

    public Product setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public Product setProductPrice(double productPrice) {
        this.productPrice = productPrice;
        return this;
    }
}
