package net.javaguides.ordersmanagement.model;

import javax.persistence.*;

@Entity
@Table(name = "order")

public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;


    public Order() {
    }

    public Order withId(int id) {
        setId(id);
        return this;
    }

    public Order build() {

        return this;
    }


    public int getId() {
        return id;
    }

    public Order setId(int id) {
        this.id = id;
        return this;
    }


}
