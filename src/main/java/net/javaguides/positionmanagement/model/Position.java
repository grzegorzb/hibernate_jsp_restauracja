package net.javaguides.positionmanagement.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="position")
public class Position {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected int id;

    @Column(name = "positionName1")
    protected String positionName1;

    @Column(name = "isDishOfTheDay")
    protected boolean isDishOfTheDay;

    @Column(name = "price")
    protected BigDecimal price;




    public Position(){

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPositionName1() {
        return positionName1;
    }

    public void setPositionName1(String positionName1) {
        this.positionName1 = positionName1;
    }


    public boolean isDishOfTheDay() {
        return isDishOfTheDay;
    }

    public void setDishOfTheDay(boolean dishOfTheDay) {
        isDishOfTheDay = dishOfTheDay;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }





    public void addPosition(int id, String positionName1, boolean isDishOfTheDay, BigDecimal price ){

        System.out.println();

    }

}
