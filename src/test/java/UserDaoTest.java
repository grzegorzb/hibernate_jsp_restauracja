import net.javaguides.usermanagement.dao.UserDao;
import net.javaguides.usermanagement.model.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class UserDaoTest {
    UserDao userDao = new UserDao();

    public void setup(){

    }

    @Test
    public void userShouldBeSaved(){
        //given
        User user = new User()
                .withEmail("user@gmail.com")
                .withName("User")
                .build();

        //when
        userDao.save(user);

        //then
        List<User> users = userDao.getAll().stream()
                .filter(u -> u.getName().equalsIgnoreCase("User"))
                .collect(Collectors.toList());

        Assert.assertEquals(users.size(),  1);
    }
}
